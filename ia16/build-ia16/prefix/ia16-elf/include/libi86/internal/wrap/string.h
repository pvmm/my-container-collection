/* Automatically generated by make-wrap-h.sh on 20220620. */

#ifndef _LIBI86_LIBI86_INTERNAL_WRAP_STRING_H_
#define _LIBI86_LIBI86_INTERNAL_WRAP_STRING_H_
#include_next <string.h>
#ifndef __STRICT_ANSI__
# include <libi86/string.h>
#endif
#endif
