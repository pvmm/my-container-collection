/* Generated automatically by the program `genconstants'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CONSTANTS_H
#define GCC_INSN_CONSTANTS_H

#define AH_REG 3
#define CH_REG 1
#define A_REG 2
#define SI_REG 8
#define LAST_HARD_REG 16
#define FIRST_NOQI_REG 8
#define UNSPEC_SEG_OVERRIDE 1
#define B_REG 6
#define LAST_ALLOCABLE_REG 12
#define DS_REG 12
#define ES_REG 11
#define UNSPEC_STATIC_FAR_PTR 3
#define DH_REG 5
#define BP_REG 10
#define BH_REG 7
#define C_REG 0
#define UNSPEC_OZSEG16 2
#define UNSPEC_DUP_SETMEM 4
#define SS_REG 15
#define ARGP_REG 17
#define CS_REG 16
#define DI_REG 9
#define CC_REG 14
#define UNSPEC_NOT_CARRY 0
#define D_REG 4
#define SP_REG 13
#define FIRST_PSEUDO_REGISTER 18

#endif /* GCC_INSN_CONSTANTS_H */
