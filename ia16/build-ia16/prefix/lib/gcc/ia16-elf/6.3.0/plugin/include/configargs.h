/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-ia16/configure --target=ia16-elf --prefix=/root/build-ia16/prefix --enable-libssp --enable-languages=c,c++ --with-newlib --disable-libstdcxx-dual-abi --disable-extern-template --disable-wchar_t --disable-libstdcxx-verbose --disable-libquadmath --with-gmp=/root/build-ia16/prefix-gmp --with-mpc=/root/build-ia16/prefix-mpc --with-mpfr=/root/build-ia16/prefix-mpfr --with-isl=/root/build-ia16/prefix-isl";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
