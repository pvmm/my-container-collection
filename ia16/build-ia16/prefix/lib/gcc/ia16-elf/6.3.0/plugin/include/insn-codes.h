/* Generated automatically by the program `gencodes'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CODES_H
#define GCC_INSN_CODES_H

enum insn_code {
  CODE_FOR_nothing = 0,
  CODE_FOR__movphi_ds_ss_slow = 8,
  CODE_FOR__movhi_ds_ss_slow = 9,
  CODE_FOR__pushphi1_nonimm = 19,
  CODE_FOR__pushhi1_nonimm = 20,
  CODE_FOR__pushv2qi1_nonimm = 21,
  CODE_FOR__pophi1_hicc = 27,
  CODE_FOR__popphi1 = 28,
  CODE_FOR__pophi1 = 29,
  CODE_FOR__xchgphi2 = 43,
  CODE_FOR__xchghi2 = 44,
  CODE_FOR__xchgqi2 = 45,
  CODE_FOR__xchgv2qi2 = 46,
  CODE_FOR__addhi3_cc_for_carry = 252,
  CODE_FOR__addqi3_cc_for_carry = 253,
  CODE_FOR__addhi3_carry = 256,
  CODE_FOR__addqi3_carry = 257,
  CODE_FOR__addhi3_carry_cc_for_carry = 260,
  CODE_FOR__addqi3_carry_cc_for_carry = 261,
  CODE_FOR__subhi3_cc_for_carry = 262,
  CODE_FOR__subqi3_cc_for_carry = 263,
  CODE_FOR__subhi3_carry = 266,
  CODE_FOR__subqi3_carry = 267,
  CODE_FOR__subhi3_carry_cc_for_carry = 268,
  CODE_FOR__subqi3_carry_cc_for_carry = 269,
  CODE_FOR__ashlhi_const1_cc_for_carry = 270,
  CODE_FOR__ashlqi_const1_cc_for_carry = 271,
  CODE_FOR__ashlhi_const1_carry = 274,
  CODE_FOR__ashlqi_const1_carry = 275,
  CODE_FOR__ashlhi_const1_carry_cc_for_carry = 276,
  CODE_FOR__ashlqi_const1_carry_cc_for_carry = 277,
  CODE_FOR__ashrhi_const1_cc_for_carry = 278,
  CODE_FOR__lshrhi_const1_cc_for_carry = 279,
  CODE_FOR__ashrqi_const1_cc_for_carry = 280,
  CODE_FOR__lshrqi_const1_cc_for_carry = 281,
  CODE_FOR__lshrhi_const1_carry = 284,
  CODE_FOR__lshrqi_const1_carry = 285,
  CODE_FOR__lshrhi_const1_carry_cc_for_carry = 286,
  CODE_FOR__lshrqi_const1_carry_cc_for_carry = 287,
  CODE_FOR_mulqi3 = 308,
  CODE_FOR_mulqihi3 = 313,
  CODE_FOR_umulqihi3 = 314,
  CODE_FOR_divmodhi4 = 316,
  CODE_FOR_divmodqi4 = 317,
  CODE_FOR_udivmodhi4 = 318,
  CODE_FOR_udivmodqi4 = 319,
  CODE_FOR_jump = 595,
  CODE_FOR_nop = 620,
  CODE_FOR_indirect_jump = 621,
  CODE_FOR_tablejump = 622,
  CODE_FOR_decrement_and_branch_until_zero = 623,
  CODE_FOR__enter = 624,
  CODE_FOR__leave = 625,
  CODE_FOR__simple_return_with_pops = 627,
  CODE_FOR_sync_lock_test_and_sethi = 628,
  CODE_FOR_sync_lock_test_and_setqi = 629,
  CODE_FOR_trunchiphi2 = 633,
  CODE_FOR_extendphihi2 = 634,
  CODE_FOR_zero_extendphihi2 = 635,
  CODE_FOR_movphi = 636,
  CODE_FOR_movhi = 637,
  CODE_FOR_movqi = 638,
  CODE_FOR_movv2qi = 639,
  CODE_FOR_movstrictqi = 640,
  CODE_FOR_pushphi1 = 641,
  CODE_FOR_pushhi1 = 642,
  CODE_FOR_pushv2qi1 = 643,
  CODE_FOR__pushhi1_prologue = 644,
  CODE_FOR__pushhi1_hicc_prologue = 645,
  CODE_FOR_pushsi1 = 646,
  CODE_FOR_pushdi1 = 647,
  CODE_FOR_pushti1 = 648,
  CODE_FOR_pushsf1 = 649,
  CODE_FOR_pushdf1 = 650,
  CODE_FOR_pushsd1 = 651,
  CODE_FOR_pushdd1 = 652,
  CODE_FOR_pushtd1 = 653,
  CODE_FOR_pushsc1 = 654,
  CODE_FOR_pushdc1 = 655,
  CODE_FOR_pushchi1 = 656,
  CODE_FOR_pushcsi1 = 657,
  CODE_FOR_pushcdi1 = 658,
  CODE_FOR_pushcti1 = 659,
  CODE_FOR_pushqi1 = 660,
  CODE_FOR_addhi3 = 661,
  CODE_FOR_subhi3 = 662,
  CODE_FOR_andhi3 = 663,
  CODE_FOR_iorhi3 = 664,
  CODE_FOR_xorhi3 = 665,
  CODE_FOR_addqi3 = 666,
  CODE_FOR_subqi3 = 667,
  CODE_FOR_andqi3 = 668,
  CODE_FOR_iorqi3 = 669,
  CODE_FOR_xorqi3 = 670,
  CODE_FOR_andv2qi3 = 671,
  CODE_FOR_iorv2qi3 = 672,
  CODE_FOR_xorv2qi3 = 673,
  CODE_FOR_addsi3 = 674,
  CODE_FOR_subsi3 = 675,
  CODE_FOR_adddi3 = 676,
  CODE_FOR_subdi3 = 677,
  CODE_FOR_addti3 = 678,
  CODE_FOR_subti3 = 679,
  CODE_FOR_ashlsi3 = 680,
  CODE_FOR_ashldi3 = 681,
  CODE_FOR_ashlti3 = 682,
  CODE_FOR_ashrsi3 = 683,
  CODE_FOR_lshrsi3 = 684,
  CODE_FOR_ashrdi3 = 685,
  CODE_FOR_lshrdi3 = 686,
  CODE_FOR_ashrti3 = 687,
  CODE_FOR_lshrti3 = 688,
  CODE_FOR_vec_shl_v2qi = 689,
  CODE_FOR_vec_shr_v2qi = 690,
  CODE_FOR_mulhi3 = 691,
  CODE_FOR_mulhisi3 = 692,
  CODE_FOR_umulhisi3 = 693,
  CODE_FOR_ashrhi3 = 694,
  CODE_FOR_ashrqi3 = 695,
  CODE_FOR_ashlhi3 = 696,
  CODE_FOR__ashrhi3 = 697,
  CODE_FOR_lshrhi3 = 698,
  CODE_FOR_rotlhi3 = 699,
  CODE_FOR_rotrhi3 = 700,
  CODE_FOR_ashlqi3 = 701,
  CODE_FOR__ashrqi3 = 702,
  CODE_FOR_lshrqi3 = 703,
  CODE_FOR_rotlqi3 = 704,
  CODE_FOR_rotrqi3 = 705,
  CODE_FOR_one_cmplphi2 = 706,
  CODE_FOR_one_cmplhi2 = 707,
  CODE_FOR_one_cmplqi2 = 708,
  CODE_FOR_one_cmplv2qi2 = 709,
  CODE_FOR_extendhisi2 = 710,
  CODE_FOR_extendqihi2 = 711,
  CODE_FOR_zero_extendqihi2 = 712,
  CODE_FOR_cstorehi4 = 713,
  CODE_FOR_cstoreqi4 = 714,
  CODE_FOR__scond = 715,
  CODE_FOR__scond_scratch = 716,
  CODE_FOR_parityhi2 = 717,
  CODE_FOR_parityqi2 = 718,
  CODE_FOR_movmemhi = 719,
  CODE_FOR_setmemhi = 720,
  CODE_FOR_cbranchhi4 = 721,
  CODE_FOR_cbranchqi4 = 722,
  CODE_FOR_call = 723,
  CODE_FOR_call_pop = 724,
  CODE_FOR_sibcall = 725,
  CODE_FOR_call_value = 726,
  CODE_FOR_call_value_pop = 727,
  CODE_FOR_sibcall_value = 728,
  CODE_FOR_prologue = 729,
  CODE_FOR_epilogue = 730,
  CODE_FOR_sibcall_epilogue = 731,
  CODE_FOR_simple_return = 732,
  CODE_FOR_seg_override_prot_mode = 733,
  CODE_FOR_seg_override = 734,
  CODE_FOR_seg16_reloc_prot_mode = 735,
  CODE_FOR_seg16_reloc = 736,
  CODE_FOR_static_far_ptr = 737,
  CODE_FOR__pushphi1_prologue = 738
};

const unsigned int NUM_INSN_CODES = 739;
#endif /* GCC_INSN_CODES_H */
